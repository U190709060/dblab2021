CREATE DEFINER=`root`@`localhost` PROCEDURE `counter`(INOUT b INT, IN inc INT)
BEGIN
	set b = b + inc;
    set inc = 50;
END