create temporary table products_below_avg
select productid, productname, price
from products
where price < (select avg(price) from products)
limit 0;/*do not use limit get all requested data*/
 
 show table status; /*for a spesific table show all the table information in the database*/
 
 drop table products_below_avg;
 
 select * from products_below_avg;